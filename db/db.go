package db

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

type DB struct {
	db *sql.DB
}

type ShortenedLink struct {
	ID          int
	OriginalURL string
	ShortURL    string
	CreatedAt   string
}

func NewDB() *DB {
	connStr := "" // add postgres link here
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	return &DB{db: db}
}

func (d *DB) Close() {
	d.db.Close()
}

func (d *DB) Ping() error {
	return d.db.Ping()
}

func (d *DB) CreateTable() error {
	_, err := d.db.Exec("CREATE TABLE IF NOT EXISTS shortened_links (id SERIAL PRIMARY KEY,original_url TEXT NOT NULL,short_url TEXT UNIQUE NOT NULL,created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP);")
	return err
}

func (d *DB) InsertLink(originalURL, shortURL string) error {
	_, err := d.db.Exec("INSERT INTO shortened_links (original_url, short_url) VALUES ($1, $2);", originalURL, shortURL)
	return err
}

func (d *DB) GetLink(shortURL string) (*ShortenedLink, error) {
	row := d.db.QueryRow("SELECT id, original_url, short_url, created_at FROM shortened_links WHERE short_url = $1;", shortURL)
	var sl ShortenedLink
	err := row.Scan(&sl.ID, &sl.OriginalURL, &sl.ShortURL, &sl.CreatedAt)
	return &sl, err
}
