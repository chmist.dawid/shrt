package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/chmist.dawid/shrt/db"
	"gitlab.com/chmist.dawid/shrt/templates"
)

func randomString(length int) string {
	chars := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	result := make([]byte, length)
	for i := 0; i < length; i++ {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return string(result)
}

func main() {
	fmt.Println("Hello, World!")
	db := db.NewDB()
	defer db.Close()
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}
	if err := db.CreateTable(); err != nil {
		log.Fatal(err)
	}

	e := echo.New()

	t := templates.New()
	e.GET("/", func(c echo.Context) error {
		t.Render(c.Response().Writer, "index.html", nil, c)
		return nil
	})
	e.GET("/:shortURL", func(c echo.Context) error {
		shortURL := c.Param("shortURL")
		sl, err := db.GetLink(shortURL)
		if err != nil {
			return c.String(http.StatusNotFound, "Not Found")
		}
		return c.Redirect(http.StatusMovedPermanently, sl.OriginalURL)
	})
	e.POST("/shorten", func(c echo.Context) error {
		originalURL := c.FormValue("url")
		shortURL := randomString(8)
		if err := db.InsertLink(originalURL, shortURL); err != nil {
			return c.String(http.StatusInternalServerError, "Internal Server Error")
		}
		return c.String(http.StatusOK, shortURL)
	})

	e.Logger.Fatal(e.Start(":8090"))
}
